using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using UnityEngine.Jobs;
using Unity.Collections;

public class JobProblem1 : MonoBehaviour
{
    [SerializeField] public int[] Array;
    void Start()
    {
        NativeArray<int> intArray = new NativeArray<int>(Array, Allocator.Persistent);
        MyJob myJob = new MyJob()
        {
            intArr = intArray,
        };
        JobHandle jobHandle = myJob.Schedule();
        jobHandle.Complete();
        for (int i = 0; i < intArray.Length; i++)
        {
            Debug.Log(intArray[i]);
        }
        intArray.Dispose();
    }
  
    public struct MyJob : IJob
    {
        public NativeArray<int> intArr;
        public void Execute()
        {
            for(int i = 0; i < intArr.Length; i++)
            {
                if (intArr[i] > 10) { intArr[i] = 0; }
            }
        }
    }
}

