using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using UnityEngine.Jobs;
using Unity.Collections;
using System;


public class JobProblem2 : MonoBehaviour
{
    [SerializeField] public Vector3[] Posotions;
    [SerializeField] public Vector3[] Velocities;

    void Start()
    {
        NativeArray<Vector3> positionsArray = new NativeArray<Vector3>(Posotions, Allocator.Persistent);
        NativeArray<Vector3> velocitiesArray = new NativeArray<Vector3>(Velocities, Allocator.Persistent);
     
        var max = Math.Max(positionsArray.Length, velocitiesArray.Length);
        NativeArray<Vector3> FinalPositionsArray = new NativeArray<Vector3>(new Vector3[max] , Allocator.Persistent);

        MyJob myJob = new MyJob()
        {
            positionsArr = positionsArray,
            velocitiesArr = velocitiesArray,
            FinalPosArr = FinalPositionsArray

        };

    
        JobHandle jobHandle = myJob.Schedule(max,0);

        jobHandle.Complete();
        for (int i = 0; i < FinalPositionsArray.Length; i++)
        {
            Debug.Log(FinalPositionsArray[i]);
        }
        FinalPositionsArray.Dispose();
        velocitiesArray.Dispose();
        positionsArray.Dispose();
    }

    public struct MyJob : IJobParallelFor
    {
        public NativeArray<Vector3> positionsArr;
        public NativeArray<Vector3> velocitiesArr;
        public NativeArray<Vector3> FinalPosArr;
        
        public void Execute(int index)
        { FinalPosArr[index] = positionsArr[index] + velocitiesArr[index]; }

    }
    
}
