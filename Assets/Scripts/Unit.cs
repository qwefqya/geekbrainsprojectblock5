using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project
{
    public class Unit : MonoBehaviour
    {
        [SerializeField] int health;
        int healingitteration = 1;

        public void Start()
        {
            ReceiveHealing(); 
        }
        public void ReceiveHealing()
        {
            if (health < 100)
                {
                    StartCoroutine(Healing());
                }
        }

            IEnumerator Healing()
            {
            while ((health < 100) & (healingitteration <=6 ))
            {
                if ((health < 100) & (health >= 95))
                {
                    health = 100;
                }
                else
                {

                    health = health + 5;
                    yield return new WaitForSeconds(0.5f);
                }
                healingitteration++;
            }
        
            }

        }
    }
