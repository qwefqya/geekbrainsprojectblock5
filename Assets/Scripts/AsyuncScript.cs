using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;
using System.Threading;

namespace Project
{
    public class AsyuncScript : MonoBehaviour
    {

        public CancellationTokenSource cancellationTokenSource;


        void Start()
        {
            

            cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancelToken = cancellationTokenSource.Token;
            CancellationToken cancelToken1 = cancellationTokenSource.Token;
            Task task1 = Task.Run(() => Unit1Async(cancelToken));
            Task task2 = Task.Run(() => Unit2Async(cancelToken1, 60));
            cancellationTokenSource.Cancel();
            cancellationTokenSource.Dispose();

        }

        async Task Unit1Async(CancellationToken cancellationToken)
        {
            await Task.Delay(1000);
            Debug.Log("Task1 finished");
            if (cancellationTokenSource.IsCancellationRequested)
                return;
        }

        async Task Unit2Async(CancellationToken cancellationToken, int frames)
        {
            while (frames > 0)
            {
                frames--;
                await Task.Yield();
            }
            Debug.Log("Task2 finished");
            if (cancellationTokenSource.IsCancellationRequested)
                return;
        }

        


    }
}